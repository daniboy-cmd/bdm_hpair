

SUBROUTINE HDEC(TGBET)
C--HIGGS DECAY WIDTHS AND BRANCHING RATIOS
 GHT=YHT/AMT*V 
SUBROUTINE SUSYCP(TGBET)
C--CALCULATION OF SUSY-COUPLINGS FROM PSEUDOSCALAR HIGGS MASS
C--AND TAN(BETA)
C--AML: LIGHT SCALAR HIGGS MASS
C--AMH: HEAVY SCALAR HIGGS MASS
C--AMA: PSEUDOSCALAR HIGGS MASS
 GHT=SA/SB
 GHT=GHT*AMT/V
SUBROUTINE SUSYCP0(TGBET)
C--CALCULATION OF SUSY-COUPLINGS FROM PSEUDOSCALAR HIGGS MASS
C--AND TAN(BETA)
C--AML: LIGHT SCALAR HIGGS MASS
C--AMH: HEAVY SCALAR HIGGS MASS
C--AMA: PSEUDOSCALAR HIGGS MASS
 GHT=DSIN(ALP)/DSIN(BET)
 GHT=GHT*AMT/V



SUBROUTINE SMCP
C--CALCULATION OF SM-COUPLINGS
 GHT=AMT/V

c I think factria is either 0 or 1
 HFAC=3.D0*AMH**2/V
 GHHH=HFAC*FACTRIA









SUBROUTINE FORMFAC(AMQ)
 COMMON/PARAM/S
 COMMON/FORM/H1,H2,HH1,HH2
 COMMON/SCALINT/C0AB,C0AC,C0AD,C0BC,C0BD,C0CD,D0ABC,D0BAC,D0ACB
  SS=S/AMQ**2
  TT=T/AMQ**2
  UU=U/AMQ**2
  R1=M1**2/AMQ**2
  R2=M2**2/AMQ**2
  T1=TT-R1
  U1=UU-R1

  CALL INISCAL(AMQ)
C--INITIALIZATION OF SCALAR INTEGRALS

 ...

c H1 = F_triangle, H2 = G_triangle
c HH1 = F_box, HH2 = G_box
 H1 =  - 4*(C0AB*SS - 4*C0AB - 2)
 H2 = 0
 HH1 = 0
 HH2 = 0

      HH1 = (16*C0AB*SS + 2*C0AC*T1*(U1 + T1 + SS + 2*R1 - 8) + 2*C0AD
     .*( - U1*T1 - U1*SS - T1**2 - 2*T1*SS - 2*T1*R1 + 8*T1 -
     .SS**2 - 2*SS*R1 + 8*SS) + 2*C0BC*U1*(U1 + T1 + SS + 2*
     .R1 - 8) + 2*C0BD*( - U1**2 - U1*T1 - 2*U1*SS - 2*U1*R1
     . + 8*U1 - T1*SS - SS**2 - 2*SS*R1 + 8*SS) + 4*D0BAC*SS
     .*( - U1 - T1 - 2*SS - 2*R1 + 8) + 2*D0ACB*( - U1**2*T1 -
     .U1*T1**2 - U1*T1*SS - 2*U1*T1*R1 + 8*U1*T1 + U1*SS*R1
     . - 2*U1*SS + T1*SS*R1 - 2*T1*SS + SS**2*R1 - 4*SS**2
     . + 2*SS*R1**2 - 12*SS*R1 + 16*SS) + 4*D0ABC*SS*( - U1
     . - T1 - 2*SS - 2*R1 + 8) + 8*SS)/SS

      HH2 = (2*C0AB*SS*(U1**2 + 4*U1*R1 - 8*U1 + T1**2 + 4*T1*R1 - 8*
     .T1 + 2*SS*R1 + 4*R1**2 - 16*R1) + 2*C0AC*T1*(U1*R1 +
     .T1**2 + 3*T1*R1 - 8*T1 + SS*R1 + 2*R1**2 - 8*R1) + 2*
     .C0CD*( - U1**3 - U1**2*T1 - 2*U1**2*R1 + 8*U1**2 - U1*T1
     .**2 + 2*U1*SS*R1 - T1**3 - 2*T1**2*R1 + 8*T1**2 + 2
     .*T1*SS*R1 + 4*SS*R1**2 - 16*SS*R1) + 2*C0AD*( - U1
     .**2*T1 - U1**2*SS - 3*U1*T1*R1 + 8*U1*T1 - 3*U1*SS*R1
     . + 8*U1*SS - T1**2*R1 - 2*T1*SS*R1 - 2*T1*R1**2 + 8*T1
     .*R1 - SS**2*R1 - 2*SS*R1**2 + 8*SS*R1) + 2*C0BC*U1*(U1
     .**2 + 3*U1*R1 - 8*U1 + T1*R1 + SS*R1 + 2*R1**2 - 8*R1))
      HH2 = (HH2
     . + 2*C0BD*( - U1**2*R1 - U1*T1**2 - 3*U1*T1*R1 + 8*U1*T1
     . - 2*U1*SS*R1 - 2*U1*R1**2 + 8*U1*R1 - T1**2*SS - 3*T1
     .*SS*R1 + 8*T1*SS - SS**2*R1 - 2*SS*R1**2 + 8*SS*R1) +
     .2*D0BAC*( - 2*U1**2*T1 - 2*U1*T1**2 - U1*T1*SS*R1 - 4*U1*
     .T1*R1 + 16*U1*T1 - U1*SS*R1**2 + 2*U1*SS*R1 - T1
     .**3*SS - 4*T1**2*SS*R1 + 8*T1**2*SS - T1*SS**2*
     .R1 - 5*T1*SS*R1**2 + 18*T1*SS*R1 - SS**2*R1**2
     . - 2*SS*R1**3 + 12*SS*R1**2 - 16*SS*R1))
      HH2 = ( HH2 + 4*
     .D0ACB*( - U1**2*T1 - U1*T1**2 - 2*U1*T1*R1 + 8*U1*T1 + U1
     .*SS*R1 + T1*SS*R1 + 2*SS*R1**2 - 8*SS*R1) + 2*
     .D0ABC*( - U1**3*SS - 2*U1**2*T1 - 4*U1**2*SS*R1 + 8*U1**2
     .*SS - 2*U1*T1**2 - U1*T1*SS*R1 - 4*U1*T1*R1 + 16*
     .U1*T1 - U1*SS**2*R1 - 5*U1*SS*R1**2 + 18*U1*SS*R1
     . - T1*SS*R1**2 + 2*T1*SS*R1 - SS**2*R1**2 - 2*SS*
     .R1**3 + 12*SS*R1**2 - 16*SS*R1))/(U1*T1 - SS*R1)

 H1 = 4*SS/3.D0
 H2 = 0
 HH1 = -4*SS/3.D0
 HH2 = 0










      !!!!!!!!!!!!!!!!
      H1 = H1*AMQ
      H2 = H2*AMQ













SUBROUTINE MATRIX(DMAT)

 COMMON/PARAM/S
 COMMON/COUP/GLT,GHT,GHLL
 COMMON/FORM/H1,H2,HH1,HH2

 FACH=1
 IF(ISFAC.EQ.1)THEN
  FACH=S/AMH**2

c AMH is the mass
c GAMH is the gamma of the complex propagator
 PROH = DCMPLX(S-AMH**2,AMH*GAMH*FACH)
 ...
 CALL FORMFAC(AMT)
 ...
c F1 is the F factors for both triangle and box
c F2 is the G factors for both triangle and box
 F1 = H1*(... + GHT*GHHH/PROH) + HH1*GHT*GHT
 F2 = H2*(... + GHT*GHHH/PROH) + HH2*GHT*GHT
 ...
 DMAT = 2.D0*(CDABS(F1)**2+CDABS(F2)**2)










DOUBLE PRECISION FUNCTION SIGB(Z,XX)
 DJAC = 1-2*EPS
 XLAM=DSQRT((S+M1**2-M2**2)**2-4.D0*S*M1**2)
 
 CALL MATRIX(DMAT)
 ALPS=ALPHAS(Q,LOOP)
 QCD=2.D0*ALPS**2/(4.D0*PI)**2
 SIGB=GEVPB*QCD*DJAC*XLAM/S**2/4096.D0/PI*DMAT * ALPS/PI








 HFAC=3.D0*AMH**2/V
 GHHH=HFAC*FACTRIA

F1 =       (H1)*(... +   (GHT)*            GHHH/     (PROH)) +      (HH1)*  (GHT)**2
F1 = (F_t*2*SS)*(... + (AMT/V)* (3.D0*AMH**2/V)/ (S-AMH**2)) + (F_b*2*SS)*(AMT/V)**2
F2 =       (H2)*(... +   (GHT)*            GHHH/     (PROH)) +      (HH2)*  (GHT)**2
F2 = (G_t*2*SS)*(... + (AMT/V)* (3.D0*AMH**2/V)/ (S-AMH**2)) + (G_b*2*SS)*(AMT/V)**2



F1 = 2*SS*(AMT/V^2) * ( (F_t)*( 3.D0*AMH**2/ (S-AMH**2)) + (F_b)*AMT )
F1 = 2*SS*(AMT/V^2) * ( (F_t)*( C_t ) + (F_b)*AMT )